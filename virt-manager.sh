#!/bin/bash
#use doas if installed

[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"

# get user
user="$(getent group 1000 | cut -d':' -f 1)"

# install virt-manager and dependencies
$ld apt install qemu-kvm libvirt-clients libvirt-daemon libvirt-daemon-system bridge-utils virtinst virt-manager -yy

#enable services systemd or openrc
$ld systemctl enable libvirtd || $ld rc-update add libvirtd default

# start services systemd or openrc
$ld systemctl start libvirtd || $ld rc-service libvirtd start

# add user to libvirt group
$ld adduser $user libvirt

# add user to libvirt-qemu group
$ld adduser $user libvirt-qemu
