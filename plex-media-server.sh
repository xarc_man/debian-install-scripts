#!/bin/bash

[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"

[ -x "$(command -v wget)" ] || $ld apt install wget -yy
[ -x "$(command -v curl)" ] || $ld apt install curl -yy

#Enable it in the repos
echo "deb https://downloads.plex.tv/repo/deb public main" | $ld tee /etc/apt/sources.list.d/plexmediaserver.list
#Add the GPG Key
curl https://downloads.plex.tv/plex-keys/PlexSign.key | $ld apt-key add -
#Install Plex
$ld apt install plexmediaserver -yy
#Enable Plex to start at boot
$ld systemctl enable plexmediaserver || $ld rc-update add plexmediaserver default
#Start Plex
$ld systemctl start plexmediaserver || $ld rc-service plexmediaserver start
