#!/bin/bash
# declare colors
red='\033[0;31m'
nc='\033[0m'

#directories
qtilesrc="$HOME/.local/src/qtile"
bindir="$HOME/.local/bin"

#either doas or sudo will work
[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"


$ld apt update -yy

$ld apt install xorg xserver-xorg python3 python3-pip python3-v-sim python-dbus-dev \
    libpangocairo-1.0-0 alsa-utils pulsemixer vifm \
    pcmanfm firefox-esr suckless-tools xwallpaper -yy

pip3 install xcffib

pip3 install --no-cache-dir cairocffi

xdg-user-dirs-update

[ -e "$qtilesrc" ] || mkdir -p $qtilesrc
[ -e "$bindir" ] || mkdir -p $bindir

git clone https://github.com/qtile/qtile.git $qtilesrc/

pip3 install $qtilesrc/. && echo "qtile start" >> $HOME/.xinitrc

printf ${red}"QTILE binary is in $bindir ...\n REBOOT to ensure $bindir is included in the PATH\n${nc} "

