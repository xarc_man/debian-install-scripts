#!/bin/bash

# this script installs the lf terminal file manager since it is not in the debian repos
# if you'd like to have previews and icons, check out my dotfiles https://gitlab.com/linuxdabbler/dotfiles

# download lf from github and place it in ~/.local/src/ and untar lf.tar.gz in ~/.local/bin/
wget -O $HOME/.local/src/lf.tar.gz https://github.com/gokcehan/lf/releases/download/r27/lf-linux-amd64.tar.gz && \
    tar -xzf $HOME/.local/src/lf.tar.gz --directory $HOME/.local/bin/

